import 'package:flutter/material.dart';
import 'Answer.dart';
import 'quiz.dart';
import 'result.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _MyAppState();
  }
}
class _MyAppState extends State<MyApp>{
  final _questions = const [
    {'questionText': 'What\'s your favourite colors ? ',
      'answers': [
        {'Text':'blue','Score': 1},
        {'Text':'red', 'Score': 2},
        {'Text':'black','Score':3},
        {'Text': 'purple','Score':4}
        ],
    },
    {'questionText': 'what\'s your favourite animal ? ',
      'answers': [
      {'Text':'tiger','Score':5},
      {'Text':'lion','Score':6},
      {'Text':'rabbit','Score': 7},
      {'Text':'hourse','Score': 8},
      ],
    },
    {'questionText': 'What\'s your favourite bird ? ',
      'answers': [
      {'Text':'kite','Score':9},
      {'Text':'peacock','Score': 10},
      {'Text':'sparrow','Score':11},
      {'Text':'parrot','Score':12},
      ],
    }
  ];
  var _questionIndex = 0;
  var _totalScore = 0;
  var i;
  void _resetQuiz(){
    setState(() {
      _questionIndex = 0;
      _totalScore = 0;
    });
  }
  void _answerQuestion(int Score){
    _totalScore += Score;
     i= _totalScore;
    setState(() {
      _questionIndex = _questionIndex + 1;
    });
}
  @override
  Widget build(BuildContext context) {

    return new MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData.dark(),
      title: 'Quiz App',
      home: Scaffold(
        appBar: new AppBar(title: new Text('Quiz App'),backgroundColor: Colors.teal,),
        body: new Stack(
            fit: StackFit.expand,
            children: <Widget>[
        new Image(image:new AssetImage("assets/happybirthdaycard.png"),
        fit: BoxFit.cover,
        color: Colors.black87,
        colorBlendMode: BlendMode.darken,
      ),
        _questionIndex < _questions.length
            ? Quiz(
            answerQuestion: _answerQuestion,
            questionIndex: _questionIndex,
          questions: _questions,
        )
            : Result(_totalScore, _resetQuiz,i),
     ],) ),
       );
  }
}

