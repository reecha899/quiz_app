import 'package:flutter/material.dart';
import 'package:quiz_app/Answer.dart';
import 'Question.dart';


class Quiz extends StatelessWidget {
  final List<Map<String, Object>> questions;
  final int questionIndex;
  final Function answerQuestion;

  Quiz({
    @required this.questions,
    @required this.answerQuestion,
    @required this.questionIndex
  });

  @override
  Widget build(BuildContext context) {
    return new  Column(
      children: [
        new Image(image:new AssetImage("assets/q1.jpg"),
          fit: BoxFit.cover,
          color: Colors.black87,
          colorBlendMode: BlendMode.darken,
        ),
        Question(
            questions[questionIndex]['questionText']
        ),
        ...(questions[questionIndex]['answers'] as List<Map<String, Object>>)
            .map((answer) {
          return Answer(() => answerQuestion(answer['Score']), answer['Text']);
        }).toList()
      ],
    );
  }
}
