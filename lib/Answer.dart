import 'package:flutter/material.dart';

class Answer extends StatelessWidget {
  final Function selectHandler;
  final String answerText;

  Answer(this.selectHandler, this.answerText);

  @override
  Widget build(BuildContext context) {
    return Center(
   child:  Container(
      margin: EdgeInsets.only(top: 10),
      padding: EdgeInsets.only(left: 40,right: 40),
      width: double.infinity,
      child: RaisedButton(
        padding: EdgeInsets.all(10.0),
        textColor: Colors.yellowAccent,
        color: Colors.pinkAccent,
        child: Text(answerText,textAlign: TextAlign.center,),
        onPressed: selectHandler,
      ),
    ),);
  }
}