import 'package:flutter/material.dart';

class Result extends StatelessWidget {
  final int resultScore;
  final Function resetHandler;
  final int total;
  Result(this.resultScore,this.resetHandler,this.total);
  String get  resultPhrase{
    String resultText;
    if(resultScore <= 8){
      resultText = 'You are awesome and innocent!';
    }
    else if(resultScore <= 12){
      resultText = 'You are Likeable!';
    }else if(resultScore <= 18)
      {
        resultText = 'Yoy are ..... Stranger?';
      }
    else{
      resultText = 'You are so bad!';
    }
    return resultText;
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child:  Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text('$total',style: TextStyle(fontSize: 30,color: Colors.lightGreen),),
      Text(
        resultPhrase,
        style: TextStyle(fontSize: 40, fontWeight: FontWeight.bold,color: Colors.lightBlue),
        textAlign: TextAlign.center,
      ),
         MaterialButton(
             height: 40.0,
             minWidth: 60.0,
           color: Colors.orangeAccent,
            child: new Text('Restart Quiz',textAlign: TextAlign.center,style: TextStyle(fontSize: 20),),
            onPressed: resetHandler,
             splashColor: Colors.blueAccent
          )
    ]),);
  }
}
