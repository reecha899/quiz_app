import 'package:flutter/material.dart';

class Question extends StatelessWidget {
  final String questionText;

  Question(this.questionText);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      padding: EdgeInsets.all(10.0),
      child: Column(children: <Widget>[
        Text(
          questionText,
          style: TextStyle(fontSize: 20,color: Colors.white),
          textAlign: TextAlign.center,
        ),

      ]),
    );
  }
}
